const path = require('path');
const utils = require('./utils');
const contextConfig = require('./webpack.common.js');
const webpackMerge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ENV = 'production';
const sass = require('sass');

module.exports = (options) => webpackMerge(contextConfig({env: ENV}), {
    mode: ENV,
    entry: [
        './src/main/webapp/app/index'
    ],
    output: {
        path: utils.root('target/classes/static'),
        filename: 'app/[name].[hash].bundle.js',
        chunkFilename: 'app/[id].[hash].chunk.js'
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.s?css$/,
                loader: 'stripcomment-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../'
                        }
                    },
                    'css-loader',
                    'postcss-loader',
                    {
                        loader: 'sass-loader',
                        options: {implementation: sass}
                    }
                ]
            }
        ]
    },
    optimization: {
        runtimeChunk: false,
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                // sourceMap: true, // Enable source maps. Please note that this will slow down the build
                terserOptions: {
                    ecma: 6,
                    toplevel: true,
                    module: true,
                    beautify: false,
                    comments: false,
                    compress: {
                        warnings: false,
                        ecma: 6,
                        module: true,
                        toplevel: true
                    },
                    output: {
                        comments: false,
                        beautify: false,
                        indent_level: 2,
                        ecma: 6
                    },
                    mangle: {
                        keep_fnames: true,
                        module: true,
                        toplevel: true
                    }
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            filename: 'content/[name].[hash].css',
            chunkFilename: 'content/[name].[hash].css'
        })
    ]
});
