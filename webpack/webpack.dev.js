const path = require('path');
const utils = require('./utils');
const contextConfig = require('./webpack.common.js');
const webpackMerge = require('webpack-merge');
const ENV = 'development';
const sass = require('sass');

module.exports = (options) => webpackMerge(contextConfig({env: ENV}), {
    devtool: 'cheap-module-source-map',
    mode: ENV,
    entry: [
        './src/main/webapp/app/index.tsx'
    ],
    output: {
        path: utils.root('target/classes/static'),
        filename: 'app/[name].bundle.js',
        chunkFilename: 'app/[id].chunk.js'
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', {
                    loader: 'sass-loader',
                    options: { implementation: sass }
                }
                ]
            }
        ]
    },
    /*devServer: {
        stats: options.stats,
        hot: true,
        contentBase: './target/classes/static'
    }*/
});
