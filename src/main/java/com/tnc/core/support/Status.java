package com.tnc.core.support;

public enum Status {
    WAITING,
    IN_PROGRESS,
    DONE,
    DELIVERING,
    DELIVERED
}
