package com.tnc.core.support;

public enum Role {
    ADMINISTRATOR,
    MANAGER,
    STAFF
}
