package com.tnc.core.models;

public class Address {
    private int number;
    private String street;
    private String district;
    private String city;
    private int postCode;

    public Address(int number, String street, String district, String city, int postCode) {
        this.number = number;
        this.street = street;
        this.district = district;
        this.city = city;
        this.postCode = postCode;
    }

    public Address() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }
}
