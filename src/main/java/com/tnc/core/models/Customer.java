package com.tnc.core.models;

import com.tnc.core.support.Role;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "customer")
public class Customer {
    @Id
    private String customerId;

    private String firstName;
    private String lastName;

    private Account account;
    private Address address;

    public Customer() {
    }

    public Customer(String firstName, String lastName, Account account, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.account = account;
        this.address = address;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
